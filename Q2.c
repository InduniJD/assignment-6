# include <stdio.h>

int fibonacciSeq(int n) {
	if(n==0)
		return 0;
	else if(n==1)
		return 1;
	else
		return fibonacciSeq(n-1) + fibonacciSeq(n-2);
}

int main() {
	int n, m;
	int i = 0;
	
	printf("Enter a number: ");
	scanf("%d", &n);
	printf("\n");
	
	for(m = 1; m <= n; m++){
		printf("%d\n", fibonacciSeq(i));
		i++;
	}
	
	return 0;
}